.PHONY: all clean run
default: all

all: yosys
	@make clean; mkdir ./obj
	g++ -I./binary/share/tcl/include/ -c src/TclInterpController.cpp -o ./obj/TclInterpController.o
	g++ -I./binary/share/tcl/include/ -c src/EpicCmdMgr.cpp -o ./obj/EpicCmdMgr.o
	g++ -I./binary/share/tcl/include/ -c src/EpicCommonCmd.cpp -o ./obj/EpicCommonCmd.o
	g++ -I./binary/share/tcl/include/ -c src/EpicExtendCmd.cpp -o ./obj/EpicExtendCmd.o
	g++ -I./binary/share/tcl/include/ -c src/EpicApp.cpp -o ./obj/EpicApp.o
	g++ -I./binary/share/readline/include/ -I./binary/share/tcl/include/ -c epic_fv_start.cpp -o ./obj/epic_fv_start.o
	g++ ./obj/TclInterpController.o ./obj/EpicCmdMgr.o ./obj/EpicCommonCmd.o ./obj/EpicExtendCmd.o ./obj/EpicApp.o ./obj/epic_fv_start.o\
	 -Lbinary/share/openmpi/lib -ltcl8.6 -lpthread -lreadline -o binary/epic_fv_start
	cp src/yosys/yosys binary/bin/.
	cp src/yosys/yosys-abc binary/bin/.

yosys:
	cd src/yosys; make -j10

clean:
	rm -rf binary/epic_fv_start ./obj

run:
	cd example/run; ../../epic_fv -f demo.tcl
