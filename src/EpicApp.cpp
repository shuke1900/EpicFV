//
// Created by xiaofoo.
//

#include "EpicApp.h"

EpicApp *EpicApp::_pinstance{nullptr};
pthread_once_t EpicApp::_ponce = PTHREAD_ONCE_INIT;

EpicApp::EpicApp() { regsiter_cmd(); }

EpicApp::~EpicApp() {}

void EpicApp::regsiter_cmd() {
  new EpicSetModeCmd("set_mode");
  new EpicSetDepthCmd("set_depth");
  new EpicSetWorkerCmd("set_worker");
  new EpicChformalCmd("chformal");
  new EpicSetEnginesCmd("set_engines");
  new EpicReadFileCmd("read_file");
  new EpicCreateClockCmd("create_clock");
  new EpicCreateRstCmd("create_rst");
  new EpicRunSimCmd("run_sim");
  new EpicCheckFvCmd("check_fv");
  new EpicExitCmd("exit");
  new EpicQuitCmd("quit");
}

void EpicApp::init() { _pinstance = new EpicApp(); }

EpicApp *EpicApp::getInstance() {
  pthread_once(&_ponce, &EpicApp::init);
  return _pinstance;
}
