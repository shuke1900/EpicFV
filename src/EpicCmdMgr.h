//
// Created by xiaofoo.
//

#ifndef EPICFV_EPICCMDMGR_H
#define EPICFV_EPICCMDMGR_H

#include <pthread.h>

#include <unordered_map>

#include "TclInterpController.h"
#include "EpicCommonCmd.h"

class EpicCommonCmd;

class EpicCmdMgr {
 private:
  static EpicCmdMgr *_pinstance;
  //    static std::mutex _mutex;
  static pthread_once_t _ponce;

  static void init();

  EpicCmdMgr();

  ~EpicCmdMgr();

  std::unordered_map<std::string, EpicCommonCmd *> cmd_map;

 public:
  EpicCmdMgr(EpicCmdMgr &other) = delete;

  void operator=(const EpicCmdMgr &) = delete;

  static EpicCmdMgr *getInstance();

  std::unordered_map<std::string, EpicCommonCmd *> getCmdMap() const;

  void addCmdMap(std::string, EpicCommonCmd *);
};

#endif  // EPICFV_EPICCMDMGR_H
