//
// Created by xiaofoo.
//

#ifndef EPICFV_EPICCOMMONCMD_H
#define EPICFV_EPICCOMMONCMD_H

#include <unordered_map>
#include <tcl.h>

#include "TclInterpController.h"
#include "EpicCmdMgr.h"

class EpicCmdMgr;

class EpicCommonCmd {
 public:
  EpicCommonCmd(const char *cmdName);

  virtual ~EpicCommonCmd();

 private:
  static int run(ClientData clientData, Tcl_Interp *interp, int argc,
                 Tcl_Obj *const argv[]);

  virtual void preExec(ClientData clientData, Tcl_Interp *interp, int argc,
                       Tcl_Obj *const argv[]);

  virtual int exec(ClientData clientData, Tcl_Interp *interp, int argc,
                   Tcl_Obj *const argv[]);

  virtual void postExec(ClientData clientData, Tcl_Interp *interp, int argc,
                        Tcl_Obj *const argv[]);
};

#endif  // EPICFV_EPICCOMMONCMD_H
