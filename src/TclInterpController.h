//
// Created by xiaofoo.
//

#ifndef EPICFV_TCLINTERPCONTROLLER_H
#define EPICFV_TCLINTERPCONTROLLER_H

#include <iostream>

#include <tcl.h>
//#include <mutex>
#include <pthread.h>

class TclInterpController {
 private:
  static TclInterpController *_pinstance;
  //    static std::mutex _mutex;

  static pthread_once_t _ponce;
  static void init();

  TclInterpController(Tcl_Interp *interp);

  ~TclInterpController();

  Tcl_Interp *_interp;

 public:
  TclInterpController(TclInterpController &other) = delete;

  void operator=(const TclInterpController &) = delete;

  static TclInterpController *getInstance();

  static void shutdown();

  static void executeCommand(const std::string &command);

  Tcl_Interp *interp() const;
};

#endif  // EPICFV_TCLINTERPCONTROLLER_H
