# usage:
# docker build --tag epicfv .
# docker run -it -w /home/unroot/EpicFV epicfv

FROM centos:8.3.2011

USER root

RUN yum -y install clang tcl-devel readline-devel
RUN yum -y install git vim
RUN yum -y install gcc gcc-c++ gdb flex bison
RUN yum -y install make libffi-devel
RUN yum -y install python36

RUN useradd -d /home/unroot unroot


USER unroot

RUN cd /home/unroot && git clone https://gitee.com/x-epic/EpicFV.git

WORKDIR /home/unroot/EpicFV

RUN cd /home/unroot/EpicFV && make

