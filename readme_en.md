![](docs/banner.png)
---

With the rapid development of the chip design industry, the industry has placed higher demands on chip verification. Dynamic simulation, formal verification, prototyping and hardware acceleration are some of the most common means of functional verification.

**Among them, formal verification has the following three advantages:** 

1. formal verification is exhaustive and complete.
2. formal verification does not require any complex test stimulus environment to be built, only constraints and test points to be written.
3. formal verification is orders of magnitude more efficient for design convergence than traditional simulation.
4. formal verification is suitable for designers to verify the functionality of their modules at an early stage.

However, formal verification requires the use of assertions (SystemVerilog Assertion) and engineers must be familiar with the syntax and the internal engine of the tool, which places high demands on the threshold of use and maturity of development skills, resulting in universities and design houses alike being deterred by a lack of experience with formal verification tools, although they would like to use them to greatly accelerate verification convergence. The lack of experience has stopped them from doing so.

**As the world's first open-source formal verification tool, "EpicFv" can automatically perform syntax parsing, design synthesis and assertion parsing when the user inputs the RTL design and SystemVerilog assertions to describe the design specification, and automatically dispatch the engine to solve the assertions based on distributed computing theory to accelerate verification. This reduces the threshold for the use of formal verification tools by automatically scheduling the engine to solve assertions based on distributed computing theory and accelerating verification convergence.**

## Overview

- [Architecture(Chinese)](docs/architecture.md)
- [Code Structure](docs/code.md)
- [Build and Run](docs/deploy.md)


## Features

■ The industry's first open source formal verification tool

■ Distributed/parallel computing to accelerate validation convergence

■ Automatic scheduling engine to reduce the threshold of using formal verification tools

■ Professional technical support in ease of use, usability and stability

## Support

- [Manual(Chinese)](docs/EPIC_FV使用手册.pdf)
- [Forum](http://edagit.com/)
